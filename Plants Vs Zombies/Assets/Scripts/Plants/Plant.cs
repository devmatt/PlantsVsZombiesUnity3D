﻿using System.Collections;
using UnityEngine;
using DG.Tweening;


public abstract class Plant : MonoBehaviour
{

	[SerializeField]
	private PlantInfo plantInfo;

	[SerializeField]
	private SpriteRenderer spriteRenderer;

	public bool isAttacked = false;

	public virtual void OnStart(){

	}

	private void Start(){


		OnStart ();

	}
	protected virtual void OnTakeDamge(){

		spriteRenderer.DOColor (new Color (1, 1, 1, 0.5f), 0.5f)
			.OnComplete (() => spriteRenderer.DOColor (new Color (1, 1, 1, 1), 0.5f));

	}

	public void TakeDamage (int damage)
	{

		if (plantInfo.health - damage <= 0)
			OnDie ();
		else {
			plantInfo.health -= damage;
			StartCoroutine (CheckSafeState ());
			OnTakeDamge ();

		}
	}

	private IEnumerator GenerateBackHealth(){

		while (plantInfo.health < 100 && !isAttacked) {

			yield return new WaitForSeconds (2);
			if (plantInfo.health + 10 > 100) {
				plantInfo.health = 100;
				yield break;
			}
			else
				plantInfo.health += 10;

		}

		yield break;

	}

	private IEnumerator CheckSafeState(){

		float t = 0;
		while (isAttacked) {

			t += Time.deltaTime;

			if (t >= 5) {
				isAttacked = false;
				StartCoroutine (GenerateBackHealth ());

				yield break;

			}
		}

	}


	private void OnDie ()
	{ // TODO:: CHANGE TO EVENT
		Destroy (this.gameObject);
	}


	public void Initialize (PlantInfo plantInfo)
	{

		this.plantInfo = plantInfo;

	}



}