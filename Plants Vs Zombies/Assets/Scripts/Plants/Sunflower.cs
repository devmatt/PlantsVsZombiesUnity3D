﻿using UnityEngine;
using System.Collections;
using DG.Tweening;



public class Sunflower : Plant {




	[SerializeField]
	private GameObject sunPrefab;


	private IEnumerator GenerateSunflowers(){
		GameManager gm = GameManager.Instance;

		while (true) {
			yield return new WaitForSeconds (gm.gameConstants.SpawnTime); // TODO:: Make object pooling
			GameObject newSun = (GameObject)Instantiate(sunPrefab,new Vector3(transform.position.x,transform.position.y,-9),Quaternion.identity);

		}

	}
	public override void OnStart(){


		StartCoroutine (GenerateSunflowers ());

	}

}
