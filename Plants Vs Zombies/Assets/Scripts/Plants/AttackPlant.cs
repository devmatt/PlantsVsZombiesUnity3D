﻿using System.Collections;
using UnityEngine;

public abstract class AttackPlant : Plant, IShootablePlant{


	[SerializeField]
	private GunPlantInfo gunPlantStats; 

	[SerializeField]
	private BulletInfo bulletInfo;

	public GunPlantInfo GunPlantStats{

		get{
			return gunPlantStats;

		}

	}

	IEnumerator IShootablePlant.Shoot(){

		while (true) {
			
			for (int i = 0; i < GunPlantStats.fireRate; i++) {
				GameObject newBullet = (GameObject)GameObject.Instantiate (GunPlantStats.bullet.gameObject, new Vector2(transform.position.x + i,transform.position.y), Quaternion.identity);
				newBullet.GetComponent<Bullet> ().InitializeBullet (bulletInfo);
			}
			yield return new WaitForSeconds (GunPlantStats.bulletRate);
		}
	}

	public override void OnStart ()
	{
		StartCoroutine (GetComponent<IShootablePlant> ().Shoot ());

	}







}