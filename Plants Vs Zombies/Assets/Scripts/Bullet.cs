﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[System.Serializable]
public class BulletInfo{

	public float bulletSpeed;
	public int bulletDamage;

}

public class Bullet : MonoBehaviour {


	private BulletInfo gunInfo;

	public void InitializeBullet(BulletInfo gunInfo){

		if (gunInfo != null) {
			this.gunInfo = gunInfo;
			StartCoroutine (Move ());
		}
	}

	public IEnumerator Move(){



		while (true) {
			transform.Translate (Vector2.right * gunInfo.bulletSpeed * Time.fixedDeltaTime);
			yield return null;

		}
	}

	public void Destroy(){ // TODO:: OBJECT POOLING
		transform.DOScale (Vector3.zero, 0.5f).OnComplete (() => Destroy (this.gameObject));
	}

	private void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.GetComponent<Enemy> () != null) {

			Enemy enemy = other.gameObject.GetComponent<Enemy> ();
			enemy.TakeDamage (gunInfo.bulletDamage);
			Destroy ();
		}
		if (other.gameObject.tag == "Destroyer") {

			Destroy ();

		}

	}


}
