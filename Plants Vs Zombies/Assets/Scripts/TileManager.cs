﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour
{

	private static TileManager _instance;

	public static TileManager Instance {

		get {

			if (_instance == null) {

				_instance = FindObjectOfType<TileManager> ();

				if (_instance == null) {

					GameObject tileManager = new GameObject ("Tile Manager");
					tileManager.AddComponent<TileManager> ();

					_instance = tileManager.GetComponent<TileManager> ();
				}
			}

			return _instance;

		}

	}



	[SerializeField]
	private List<Tile> tiles = new List<Tile> ();

	[SerializeField]
	private GameObject tilePrefab;

	[SerializeField]
	private Vector2 startPosition;

	[SerializeField]
	private TileCoordinates rows;

	[SerializeField]
	private Vector2 tileOffset;

	public int xBounds;
	public int yBounds;

	private GameObject tileParent;

	private void CreateTile (float x, float y, TileCoordinates tileCoords)
	{

		GameObject newTile = (GameObject)GameObject.Instantiate (tilePrefab, new Vector2 (x, y), Quaternion.identity);
		Tile tile = newTile.GetComponent<Tile> ();
		tile.Coordinates = tileCoords;
		BoxCollider2D collider = newTile.GetComponent<BoxCollider2D> ();
		tile.Coordinates.worldPosition = new Vector2 (x, y);
		newTile.name = "Tile (" + tile.Coordinates.x + ":" + tile.Coordinates.y + ")";
		tiles.Add (tile);

		if (tileParent != null)
			newTile.transform.SetParent (tileParent.transform);
		else {

			tileParent = new GameObject ("Tiles");
			newTile.transform.SetParent (tileParent.transform);

		}

	}

	public Vector2 GetVectorByCoordinates (TileCoordinates tileCoordinates)
	{

		if (tileCoordinates.x > 0 || tileCoordinates.x <= xBounds && tileCoordinates.y > 0 || tileCoordinates.y <= yBounds && tileCoordinates.worldPosition != null) {
			for (int i = 0; i < tiles.Count; ++i)
				if (tiles [i].GetX == tileCoordinates.x || tiles [i].GetY == tileCoordinates.y)
					return tileCoordinates.worldPosition;

			
		} 
		Debug.LogError ("Coordinates out of bounds.");
		return Vector2.zero;

	}

	public TileCoordinates GetTileCoordinates (int x, int y)
	{

		if (x > 0 || x <= xBounds && y > 0 || y <= yBounds) {

			for (int i = 0; i < tiles.Count; i++) {

				if (tiles [i].Coordinates.x == x && tiles [i].Coordinates.y == y)
					return tiles [i].Coordinates;

			}

		} 

		return null;



	}

	public float GetRandomColummPosition ()
	{

		int ranId = Random.Range (0, yBounds);
		return GetTileCoordinates (0, ranId).worldPosition.y;

	}

	// Generates a grid of tiles.
	private void GenerateGrid ()
	{

		if (tileParent == null)
			tileParent = new GameObject ("Tiles");


		for (int i = 0; i < rows.x; i++) {


			for (int j = 0; j < rows.y; j++) {

				float newX = startPosition.x + (i * tileOffset.x);
				float newY = startPosition.y + (j * tileOffset.y);
				CreateTile (newX, newY, new TileCoordinates (i, j));

					

			}

		}
		xBounds = rows.x;
		yBounds = rows.y;
	}


	private void Start ()
	{

		GenerateGrid ();



	}


}
