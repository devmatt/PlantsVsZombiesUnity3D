﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[System.Serializable]
public class ZombieStats{

	public int Health;
	public int Damage;
	public float MovingSpeed;
	public int Strength;
	public float AttackSpeed;

	public ZombieStats(int Health, int Damage,float MovingSpeed, int Strength, float AttackSpeed){

		this.Health = Health;
		this.Damage = Damage;
		this.MovingSpeed = MovingSpeed;
		this.Strength = Strength;
		this.AttackSpeed = AttackSpeed;
	}

	public ZombieStats(){

		Health = 100;
		Damage = 1;
		MovingSpeed = 0.1f;
		Strength = 1;
		AttackSpeed = 1f;
		
	}

}

public abstract class Enemy : MonoBehaviour{

	[SerializeField]
	private ZombieStats Stats;

	public bool isEating = false;

	[SerializeField]
	private SpriteRenderer spriteRenderer;

	public Plant currentEatingPlant = null;

	public int GetHealth{

		get{

			return Stats.Health;
		}

	}

	public int GetDamage {
		get {

			return Stats.Damage;

		}

	}

	public float GetMovingSpeed {
		
		get {

			return Stats.MovingSpeed;

		}

	}


	protected virtual void Attack(){

		currentEatingPlant.TakeDamage (Stats.Damage);
	
	}

	public virtual IEnumerator StartAttacking(Plant plant){

		if (plant != null) {

			Stats.MovingSpeed = 0;
			isEating = true;
			currentEatingPlant = plant;
			plant.isAttacked = true; //TODO:: Make Plant and zombie states methods that transition rather than typing shit tons of spagghetii.
			while (isEating && currentEatingPlant !=null) {

				Attack ();
				yield return new WaitForSeconds (Stats.AttackSpeed);
			}

			Stats.MovingSpeed = 1;
			isEating = false;
			currentEatingPlant = null;

		}
	}

	protected virtual void OnTakeDamge(){

		spriteRenderer.DOColor (new Color (1, 1, 1, 0.5f), 0.5f)
			.OnComplete (() => spriteRenderer.DOColor (new Color (1, 1, 1, 1), 0.5f));

	}

	public virtual void TakeDamage(int damage){

		if (GetHealth - damage > 0) {
			Stats.Health -= damage;
			OnTakeDamge ();
			return;
		}

		Die ();


	}

	public virtual void Die(){ 

		EnemySpawner.Instance.OnKilled (this);
		Destroy (this.gameObject);

	}

	private void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.GetComponent<Plant> () != null) {

			StartCoroutine (StartAttacking (other.gameObject.GetComponent<Plant>()));

		}

	}

	public virtual IEnumerator Move(){

		while (Input.GetKey(KeyCode.A)) {

			transform.Translate (Vector2.left * GetMovingSpeed * Time.deltaTime);
			yield return null;
		
		}
		yield break;

	}



	private void Start(){

		StartCoroutine (Move ());

	}





}