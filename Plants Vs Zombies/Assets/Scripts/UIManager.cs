﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour{


	private static UIManager _instance;

	public static UIManager Instance{

		get{

			if (_instance == null) {

				_instance = FindObjectOfType<UIManager> ();

				if (_instance == null) {

					GameObject uiManager = new GameObject ("UI Manager");
					uiManager.AddComponent<UIManager> ();

					_instance = uiManager.GetComponent<UIManager> ();
				}
			}
			return _instance;

		}


	}

	private void UpdateSunflowersText(){

		sunflowersText.text = GameManager.Instance.currentSunflowers.ToString ();

	}

	private void OnEnable(){

		OnUpdateUI += UpdateSunflowersText;

	}
	private void OnDisable(){

		OnUpdateUI -= UpdateSunflowersText;

	}

	public delegate void UpdateUI ();
	public static event UpdateUI OnUpdateUI;

	public static void RefreshUI(){

		if (OnUpdateUI != null)
			OnUpdateUI ();

	}

	private void Start(){

		RefreshUI ();

	}

	[SerializeField]
	private Text sunflowersText;

	//public void UpdateUI(){ // DO EVENT INSTEAD 
	//	GameManager gm = GameManager.Instance;

	//	sunflowersText.text = gm.currentSunflowers.ToString ();

	//}



}