﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class TileCoordinates{

	public int x;
	public int y;
	public Vector2 worldPosition;

	public TileCoordinates(int x,int y){

		this.x = x;
		this.y = y;

	}

}

public class Tile : MonoBehaviour {

	[SerializeField]
	private TileCoordinates coordinates;

	public Plant currentPlant;

	public int GetX{

		get{
			return coordinates.x;
		}
	

	}

	public TileCoordinates Coordinates{

		get{

			return coordinates;
		}
		set{

			coordinates = value;
		}

	}

	public int GetY{
	
		get{
			return coordinates.y;
		}

	}

}
