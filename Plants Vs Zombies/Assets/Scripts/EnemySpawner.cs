﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Wave
{

	public int amountOfZombies;
	public float spawnRate;
	public List<Enemy> enemies;
	public float delayTime;

}

public class EnemySpawner : MonoBehaviour
{

	private static EnemySpawner _instance;

	public static EnemySpawner Instance {

		get {

			if (_instance == null) {

				_instance = FindObjectOfType<EnemySpawner> ();

				if (_instance == null) {

					GameObject enemySpawner = new GameObject ("Enemy Spawner");
					enemySpawner.AddComponent<EnemySpawner> ();

					_instance = enemySpawner.GetComponent<EnemySpawner> ();
				}
			}

			return _instance;

		}

	}



	[SerializeField]
	private int zombiesLeft;

	[SerializeField]
	private float spawnRate;

	[SerializeField]
	private List<Wave> waves;

	[SerializeField]
	private GameObject spawnPoint;

	[SerializeField]
	private List<Enemy> currentEnemies;

	private void InitializeWave (Wave wave)
	{

		zombiesLeft = wave.amountOfZombies;
		spawnRate = wave.spawnRate;

	}

	private void SpawnZombie (Wave wave)
	{
		// TODO:: MAKE SPAWNING RANDOM BASED ON THEIR STRENGTH - MORE STRENGTH = MORE RARE

		GameObject newZombie = (GameObject)GameObject.Instantiate (wave.enemies [Random.Range (0, wave.enemies.Count)].gameObject, new Vector2 (spawnPoint.transform.localPosition.x, TileManager.Instance.GetRandomColummPosition ()), Quaternion.identity);
		currentEnemies.Add (newZombie.GetComponent<Enemy> ());


	}

	private void Start ()
	{

		StartCoroutine (StartSpawner ());

	}
		
	public void OnKilled(Enemy enemy){

		currentEnemies.Remove (enemy);
	}

	public IEnumerator StartSpawner ()
	{

		for (int i = 0; i < waves.Count; ++i) {

			int zombiesSpawned = 0;

			InitializeWave (waves [i]);

			yield return new WaitForSeconds (waves [i].delayTime);

			while (currentEnemies.Count >= waves[i].amountOfZombies || currentEnemies.Count == 0) {

				if (currentEnemies.Count >= waves [i].amountOfZombies) {
					yield return null;

				} else {
					yield return new WaitForSeconds (spawnRate);
					SpawnZombie (waves [i]);
				}
			}


		


		}
		yield break;



	}




}
